from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Post
from .forms import PostForm

def index(request):
    if request.method =="POST":
        form=PostForm(request.POST)
        if form.is_valid:
            post=form.save()
            post.save()
            return redirect('/')
    else:
        form=PostForm()
    return render(request, 'index.html', {'form':form, 'postlist':Post.objects.order_by("-date")})
        # Create your views here.
