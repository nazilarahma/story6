from django.test import TestCase, Client
from django.urls import resolve
from homepage.views import index
from homepage.models import Post
from homepage.forms import PostForm
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time

class HomepageUnittest(TestCase):

    def setUp(cls):
        Post.objects.create(status="Recommended")
    
    def test_show_url_is_exist(self):
        response= Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        
    def test_show_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_if_models_in_database(self):
        statusData = Post.objects.create(status="mantap bgt")
        count_statusData = Post.objects.all().count()
        self.assertEqual(count_statusData, 2)

    def test_if_statusData_status_is_exist(self):
        statusDataObj= Post.objects.get(id=1)
        statusObj=statusDataObj._meta.get_field('status').verbose_name
        self.assertEqual(statusObj, 'status')

    def test_if_statusData_date_is_exist(self):
        statusDataObj= Post.objects.get(id=1)
        statusObj=statusDataObj._meta.get_field('date').verbose_name
        self.assertEqual(statusObj, 'date')

    def test_form_input_html(self):
        form=PostForm()
        self.assertIn('id="id_status', form.as_p())

    def test_form(self):
        form=PostForm(data={'status':'ada status'})
        self.assertTrue(form.is_valid())
        request=self.client.post('/', data={'status':'ada status'})
        self.assertEqual(request.status_code, 302)

        response=self.client.get('/')
        self.assertEqual(response.status_code, 200)


class FunctionalTestStory6(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTestStory6, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestStory6, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')
        status.send_keys('Coba Coba')
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn("Coba Coba", selenium.page_source)




        
                
	
        
    