from django import forms
from .models import Post


class PostForm(forms.ModelForm):
    status=forms.CharField(max_length=300, widget=forms.Textarea)
    class Meta:
        model=Post
        fields=['status']