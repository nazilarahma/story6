from django.conf import settings
from django.db import models
from datetime import datetime

# Create your models here.
class Post(models.Model):
    status=models.CharField(max_length=300)
    date= models.DateTimeField(default=datetime.now, blank=True)
 #
    def __str__(self):
        return self.nama