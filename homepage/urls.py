from django.contrib import admin
from . import views
from django.urls import path

app_name='homepage'

urlpatterns = [
    path('', views.index, name='index'),
    # path('', views.redirecting, name='redirecting')
]
